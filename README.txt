CharacterController2D script downloaded from: https://github.com/Brackeys/2D-Character-Controller
Sprite and Tile Assets downloaded from: https://kenney.nl/assets/simplified-platformer-pack
Music downloaded from: https://opengameart.org/content/platformer-game-music-pack
Jumping and Walking sound effects downloaded from: https://opengameart.org/content/library-of-game-sounds
Dying sound effect downloaded from: https://opengameart.org/content/oldschool-win-and-die-jump-and-run-sounds

